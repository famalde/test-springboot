package com.gatosoft.mapper.mappers;

import org.mapstruct.Mapper;
import org.mapstruct.Mapping;
import org.mapstruct.factory.Mappers;

import com.gatosoft.mapper.mapper.bean.Car;
import com.gatosoft.mapper.mapper.dto.CarDTO;

@Mapper
public interface CarMapper {

	CarMapper INSTANCE = Mappers.getMapper(CarMapper.class);
	
	@Mapping(source="numberOfSeats", target="seatCount")
	CarDTO adaptCarToCarDTO(final Car source);
}
