package com.gatosoft.mapper;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.gatosoft.mapper.enums.CarType;
import com.gatosoft.mapper.mapper.bean.Car;
import com.gatosoft.mapper.mapper.dto.CarDTO;
import com.gatosoft.mapper.mappers.CarMapper;

@RestController
public class MapperController {

	@GetMapping(path="/mapper/getCar")
	public  CarDTO getCarDTOFromCarBean() {
		final Car car = new Car();
		car.setMake("Seat");
		car.setNumberOfSeats(5);
		car.setType(CarType.Compacto);
		
		CarDTO carDTO = CarMapper.INSTANCE.adaptCarToCarDTO(car);
		return carDTO;
	}
}
